# SPDX-License-Identifier: LGPL-2.1-or-later
# https://github.com/mdealencar/interarray

import functools
import math
import operator
from collections import defaultdict
from itertools import chain, product
from math import isclose
from typing import Tuple
import shapely as shp
import networkx as nx
import numpy as np
from scipy.sparse import coo_array
from scipy.sparse.csgraph import minimum_spanning_tree as scipy_mst
from scipy.spatial import Delaunay
from scipy.spatial.distance import cdist


def make_graph_metrics(G):
    """
    The function is based on the functions developed in interarry module.
    This function changes G in place!
    Calculates for all nodes, for each root node:
    - distance to root nodes
    - angle wrt root node

    Any detour nodes in G are ignored.
    """
    VertexC = G.graph['VertexC']
    M = G.graph['M']
    # N = G.number_of_nodes() - M
    roots = range(-M, 0)
    NodeC = VertexC[:-M]
    RootC = VertexC[-M:]

    # calculate distance from all nodes to each of the roots
    d2roots = cdist(VertexC[:-M], VertexC[-M:])

    angles = np.empty_like(d2roots)
    for n, nodeC in enumerate(NodeC):
        nodeD = G.nodes[n]
        # assign the node to the closest root
        nodeD['root'] = -M + np.argmin(d2roots[n])
        x, y = (nodeC - RootC).T
        angles[n] = np.arctan2(y, x)
    # TODO: ¿is this below actually used anywhere?
    # assign root nodes to themselves (for completeness?)
    for root in roots:
        G.nodes[root]['root'] = root

    G.graph['d2roots'] = d2roots
    G.graph['d2rootsRank'] = np.argsort(np.argsort(d2roots, axis=0), axis=0)
    G.graph['angles'] = angles
    G.graph['anglesRank'] = np.argsort(np.argsort(angles, axis=0), axis=0)
    G.graph['anglesYhp'] = angles >= 0.
    G.graph['anglesXhp'] = abs(angles) < np.pi / 2


def check_crossings(G, MARGIN=0.1):
    """
    The function is based on the functions developed in interarry module.
    Check for crossings between edges in a graph.

    Parameters:
        G (networkx.Graph): The input graph.
        MARGIN (float): The threshold for considering edges as crossing.
            Defaults to 0.1.

    Returns:
        bool: True if any crossings are detected, False otherwise.
    """
    # Extract necessary information from the graph
    VertexC = G.graph['VertexC']
    M = G.graph['M']
    N = G.number_of_nodes() - G.graph.get('D', 0)
    D = G.graph.get('D')

    # Adjust the number of nodes if D is not None
    if D is not None:
        N -= D
        fnT = G.graph['fnT']
        AllnodesC = np.vstack((VertexC[:N], VertexC[fnT[N:N + D]], VertexC[-M:]))
    else:
        fnT = np.arange(N + M)
        AllnodesC = VertexC

    # Define roots
    roots = range(-M, 0)
    fnT[-M:] = roots

    # Check for crossings
    for root in roots:
        edges = list(nx.edge_bfs(G, source=root))
        for i, (u, v) in enumerate(edges):
            for s, t in edges[(i + 1):]:
                if is_crossing(*AllnodesC[[u, v, s, t]], touch_is_cross=True):
                    return True  # Crossing detected
    return False  # No crossing detected


def is_crossing(u, v, w, y, touch_is_cross=True):
    """
    The function is based on the functions developed in interarry module.
    Check if the line segment (u, v) crosses (w, y).
    Parameters:
        u, v, w, y (float): Coordinates of the line segments.
        touch_is_cross (bool): Whether touching is considered as crossing.

    Returns:
        bool: True if there is a crossing, False otherwise.
    """
    less = operator.lt if touch_is_cross else operator.le

    # Calculate differences
    A = v - u
    B = w - y

    # Bounding box check
    for i in (0, 1):  # X and Y
        lo, hi = (v[i], u[i]) if A[i] < 0 else (u[i], v[i])
        if (B[i] > 0 and (hi < y[i] or w[i] < lo)) or (B[i] < 0 and (hi < w[i] or y[i] < lo)):
            return False

    # Calculate denominator
    f = B[0] * A[1] - B[1] * A[0]
    if np.isclose(f, 0, atol=1e-3):
        return False

    # Calculate numerators and check if crossing
    for num, den in ((Px * Qy - Py * Qx, f) for (Px, Py), (Qx, Qy) in ((u - w, B), (A, u - w))):
        if (f > 0 and (less(num, 0) or less(f, num))) or (f < 0 and (less(0, num) or less(num, f))):
            return False
    return True


def is_graph_connected(G):
    """
    Check if the graph is connected (all nodes are connected).

    Parameters:
        G (networkx.Graph): The input graph.

    Returns:
        bool: True if the graph is connected, False otherwise.
    """
    # Use Depth-First Search (DFS) to traverse the graph
    visited = set()

    def dfs(node):
        visited.add(node)
        for neighbor in G.neighbors(node):
            if neighbor not in visited:
                dfs(neighbor)

    # Start DFS from an arbitrary node
    arbitrary_node = next(iter(G.nodes), None)
    if arbitrary_node is None:
        # Graph is empty, so technically it's connected
        return True

    dfs(arbitrary_node)

    # Check if all nodes are visited
    return len(visited) == len(G.nodes)


def is_inside_boundary(boundary, coordinates):
    """
    Check if all turbine coordinates are inside the defined boundary.

    Args:
    - boundary (list): List of boundary coordinates [(x1, y1), (x2, y2), ..., (xn, yn)] forming a closed polygon.
    - turbine_coordinates (list): List of turbine coordinates [(x1, y1), (x2, y2), ..., (xn, yn)].

    Returns:
    - bool: True if all turbine coordinates are inside the boundary, False otherwise.
    """

    # Function to check if a point (x, y) is inside a polygon defined by vertices
    def is_inside_polygon(x, y, vertices):
        """
        Check if a point (x, y) is inside a polygon defined by vertices.

        Args:
        - x (float): x-coordinate of the point.
        - y (float): y-coordinate of the point.
        - vertices (list): List of vertex coordinates [(x1, y1), (x2, y2), ..., (xn, yn)] forming a closed polygon.

        Returns:
        - bool: True if the point is inside the polygon, False otherwise.
        """
        # Initialize variables
        inside = False
        j = len(vertices) - 1

        # Loop through each edge of the polygon
        for i in range(len(vertices)):
            xi, yi = vertices[i]
            xj, yj = vertices[j]

            # Check if the point is inside the edge
            if (yi < y <= yj or yj < y <= yi) and (xi <= x or xj <= x):
                if xi + (y - yi) / (yj - yi) * (xj - xi) < x:
                    inside = not inside
            j = i

        return inside

    # Check if all turbine coordinates are inside the boundary
    for coordinate in coordinates:
        x, y = coordinate
        if not is_inside_polygon(x, y, boundary):
            return False

    return True


def graph_to_table(G):
    """Convert graph to table format."""
    M = G.graph['M']
    Ne = G.number_of_edges()

    def edge_parser(edges):
        for u, v, data in edges:
            s = (u + M + 1) if u >= 0 else abs(u)
            t = (v + M + 1) if v >= 0 else abs(v)
            yield (s, t, data['length'], data.get('load', 0), data.get('cable', 0), data.get('cost', 0))

    T = np.fromiter(edge_parser(G.edges(data=True)),
                    dtype=[('u', int), ('v', int), ('length', float),
                           ('load', int), ('cable', int), ('cost', float)],
                    count=Ne)
    return T


def graph_to_yaml(G):
    """Convert graph to YAML format."""
    graph_data = {
        'nodes': [{'id': node, **data} for node, data in G.nodes(data=True)],
        'edges': [{'source': u, 'target': v, **data} for u, v, data in G.edges(data=True)]
    }
    return yaml.dump(graph_data)
