import matplotlib.pyplot as plt
import yaml
from abc import ABC, abstractmethod
# from ed_win.collection_system import collection_system
# from ed_win.c_mst_cables import plot_network
from ed_win.plotting import gplot
import pandas as pd
import numpy as np
import networkx as nx
import os
import xarray as xr
from windIO.utils.yml_utils import load_yaml, Loader
from ed_win.drivers.drivers_api import InterArray
from ed_win.drivers.interarray.interface import assign_cables
from ed_win.drivers.interarray.utils import NodeTagger
from ed_win.edwin_lib import make_graph_metrics, check_crossings, is_graph_connected, is_inside_boundary, graph_to_table, graph_to_yaml
from ed_win.drivers.interarray.interarraylib import calcload
from ed_win.drivers.ga.GA import GA

F = NodeTagger()


class Driver(ABC):
    def __init__(self, **kwargs):
        '''
        '''

    def run(self, G=None):
        '''
        G: Networkx graph
        '''
        G = self._run(G=G)
        return G

    @abstractmethod
    def _run():
        '''

        '''


class InterArrayDriver(Driver):
    def __init__(self, solver_name='heuristic(cpew)', gap=0.01, timelimit=600, other_settings={}, **kwargs):
        self.solver_name = solver_name
        self.gap = gap
        self.timelimit = timelimit
        self.other_settings = other_settings
        self.supports_constraints = False
        self.supports_primary = True
        self.supports_secondary = False
        Driver.__init__(self, **kwargs)

    def _run(self, G, **kwargs):
        H = InterArray(G,
                       self.solver_name,
                       self.gap,
                       self.timelimit,
                       self.other_settings,
                       self.wfn.cables).H
        return H


class GeneticAlgorithmDriver(Driver):
    def __init__(self, WindFarm=None, settings={}, verbose=False, **kwargs):
        self.WindFarm = WindFarm
        self.settings = settings
        self.verbose = verbose
        self.supports_constraints = False
        self.supports_primary = True
        self.supports_secondary = False
        Driver.__init__(self, **kwargs)

    def _run(self, G, **kwargs):

        H = GA(G, wf=self.WindFarm, settings=self.settings, verbose=self.verbose).H
        return H


class WindFarmNetwork(nx.Graph):
    def __init__(self, turbines_pos=None, substations_pos=None, drivers=None, cables=[], site_info={}, G=None, sequence=None):
        """WindFarmNetwork object"""
        super().__init__()  # Initialize as a NetworkX graph
        if turbines_pos is not None and substations_pos is not None:
            self.G = self._create_graph(turbines_pos, substations_pos, cables, site_info)
        else:
            self.G = G
        self.drivers = drivers or [InterArrayDriver(solver_name='heuristic(cpew)', other_settings={})]
        self.cables = cables
        self.sequence = sequence or range(len(self.drivers))
        self.setup()

    @classmethod
    def from_windIO(cls, yaml_path, drivers=None, cables=None, sequence=None):
        """Create WindFarmNetwork instance from windIO YAML file."""
        windIO_data = cls._load_windIO_data(yaml_path)
        turbines_pos, substations_pos = cls._extract_positions(windIO_data)
        site_info = cls._extract_site_info(windIO_data)
        G = cls._create_graph(turbines_pos, substations_pos, cables, site_info)
        return cls(G=G, drivers=drivers, cables=cables, sequence=sequence)

    @classmethod
    def from_graph(cls, graph, drivers=None, cables=None, G=None, sequence=None):
        """Create WindFarmNetwork instance from a NetworkX graph."""
        return cls(G=graph, drivers=drivers, cables=cables, sequence=sequence)

    @staticmethod
    def _load_windIO_data(yaml_path):
        """Load windIO YAML data."""

        # Custom constructor for netCDF data
        def includeBathymetryNetCDF(self, node):
            filename = os.path.join(self._root, self.construct_scalar(node))
            dataset = xr.open_dataset(filename)
            bathymetry_data = {variable: list(dataset[variable].values.reshape(-1))
                               for variable in dataset.variables}
            return bathymetry_data

        Loader.includeBathymetryNetCDF = includeBathymetryNetCDF
        Loader.add_constructor('!includeBathymetryNetCDF',
                               Loader.includeBathymetryNetCDF)

        if not os.path.exists(yaml_path):
            raise FileNotFoundError(f"YAML file '{yaml_path}' not found.")
        return load_yaml(yaml_path, Loader)

    @staticmethod
    def _extract_positions(windIO_data):
        """Extract turbine and substation positions from windIO data."""
        try:
            turbines_x = windIO_data['wind_farm']['layouts']['initial_layout']['coordinates']['x']
            turbines_y = windIO_data['wind_farm']['layouts']['initial_layout']['coordinates']['y']
            turbines_pos = np.array([(x, y) for x, y in zip(turbines_x, turbines_y)])

            substations_x = windIO_data['wind_farm']['electrical_substations']['coordinates']['x']
            substations_y = windIO_data['wind_farm']['electrical_substations']['coordinates']['y']
            substations_pos = np.column_stack((substations_x, substations_y))
        except KeyError as e:
            raise ValueError(f"Required data '{e.args[0]}' not found in YAML file.")
        return turbines_pos, substations_pos

    @classmethod
    def _extract_site_info(cls, windIO_data):
        """Extract site information from windIO data."""
        site_data = windIO_data.get('site', {})
        site_name = site_data.get('name', None)
        handle = site_data.get('handle', None)
        boundary_data = site_data.get('boundaries', {}).get('polygons', None)
        boundary = np.array([[x, y] for x, y in zip(boundary_data[0]['x'], boundary_data[1]['y'])]) if boundary_data else None
        return {'site_name': site_name, 'handle': handle, 'boundary': boundary}

    @staticmethod
    def _create_graph(turbines_pos, substations_pos, cables, site_info={}):
        """Create a network graph from turbine and substation positions."""
        M = substations_pos.shape[0]
        VertexC = np.r_[turbines_pos, substations_pos[::-1]]
        N = len(VertexC) - M

        # Set default site information if not provided
        site_info = site_info
        site_info.setdefault('handle', 'G_from_site')
        site_info.setdefault('name', site_info.get('handle'))

        # Create the network graph
        G = nx.Graph(
            M=M,
            VertexC=VertexC,
            boundary=site_info.get('boundary'),
            name=site_info['name'],
            handle=site_info['handle'],
            cables={'area': cables[:, 0], 'capacity': cables[:, 1], 'cost': cables[:, 2]}
        )

        # Add nodes for turbines and substations
        G.add_nodes_from(((n, {'label': F[n], 'type': 'wtg'})
                          for n in range(N)))
        G.add_nodes_from(((r, {'label': F[r], 'type': 'oss'})
                          for r in range(-M, 0)))

        return G

    def setup(self):
        """Set up drivers."""
        for driver in self.drivers:
            setattr(driver, 'wfn', self)

    def optimize(self, turbines_pos=None, substations_pos=None, drivers=None, cables=[], site_info={}, G=None, sequence=None, output_format='graph',
                 **kwargs):
        """Design the wind farm network."""
        G = G if G is not None else self.G
        if turbines_pos is not None or substations_pos is not None:
            M = substations_pos.shape[0] if substations_pos is not None else G.graph['M']
            turbines_pos = turbines_pos if turbines_pos is not None else G.graph['VertexC'][0:-M]
            substations_pos = substations_pos if substations_pos is not None else G.graph['VertexC'][-M:][::-1]
            VertexC = np.r_[turbines_pos, substations_pos[::-1]]
            G.graph['VertexC'] = VertexC
            G.graph['M'] = M
            N = len(VertexC) - M
            # Remove previously added nodes for turbines and substations
            nodes_to_remove = [n for n in G if 'type' in G.nodes[n] and G.nodes[n]['type'] in ['wtg', 'oss']]
            G.remove_nodes_from(nodes_to_remove)

            # Add new nodes for turbines
            G.add_nodes_from(((n, {'label': F[n], 'type': 'wtg'}) for n in range(N)))

            # Add new nodes for substations
            G.add_nodes_from(((r, {'label': F[r], 'type': 'oss'}) for r in range(-M, 0)))

        if cables:
            G.graph['cables'] = {'area': cables[:, 0], 'capacity': cables[:, 1], 'cost': cables[:, 2]}
        if site_info:
            G.graph['site_info'].update(site_info)
        if sequence is not None:
            self.sequence = sequence

        if drivers is not None:
            self.drivers = drivers

        if G.graph['boundary'] is not None:
            inside_boundary = is_inside_boundary(boundary=G.graph['boundary'], coordinates=G.graph['VertexC'])
            if not inside_boundary:
                print('WARNING: at least one of the coordinates is outside the boundary')

        for n, driver_no in enumerate(self.sequence):
            driver = self.drivers[driver_no]
            if n == 0 and not driver.supports_primary:
                raise ValueError(f"{driver} cannot be the first driver in a sequence")
            elif n > 0 and not driver.supports_secondary:
                raise ValueError(f"{driver} cannot be used as a secondary driver")
            G = driver.run(G=G)
            self.G = G

        # Attach a plot method to the graph object
        def plot_graph(node_tag=None):
            ax = gplot(G, node_tag=node_tag)
            return ax

        G.plot = plot_graph

        if output_format == 'graph':
            output = G
        elif output_format == 'table':
            output = graph_to_table(G)
        elif output_format == 'yaml':
            output = graph_to_yaml(G)
        else:
            raise ValueError("Invalid output format. Supported formats: 'graph', 'table', 'yaml'")
        return output

    def evaluate(self, turbines_pos=None, substations_pos=None, output_format='graph', update_selfG=False, **kwargs):
        """Evaluates the layout with new coordinates."""
        G = self.G.copy()
        M = substations_pos.shape[0] if substations_pos is not None else G.graph['M']

        # Check if the graph has any edges
        if len(G.edges) == 0:
            raise ValueError("The graph (G) is empty. \n"
                             "Either run the optimize() before calling evaluate or "
                             " instantiate the class with a graph (G) which includes a layout (at least one edge)")

        #
        if not is_graph_connected(G):
            print('WARNING: the layout has some disconned nodes!')

        # Check if the number of turbines and substations matches with the graph
        if turbines_pos is not None and len(turbines_pos) != len(G.graph['VertexC']) - M:
            raise ValueError("Number of turbines in input parameters does not match with the graph.")
        if substations_pos is not None and len(substations_pos) != M:
            raise ValueError("Number of substations in input parameters does not match with the graph.")

        turbines_pos = turbines_pos if turbines_pos is not None else G.graph['VertexC'][0:-M]
        substations_pos = substations_pos if substations_pos is not None else G.graph['VertexC'][-M:][::-1]
        vertexes = np.r_[turbines_pos, substations_pos[::-1]]
        G.graph['VertexC'] = vertexes
        make_graph_metrics(G)
        for u, v, edgeD in G.edges(data=True):
            # Calculate new edge length based on updated coordintes
            edgeD['length'] = ((vertexes[v][0] - vertexes[u][0])**2 + (vertexes[v][1] - vertexes[u][1])**2)**0.5
            edgeD['cost'] = edgeD['length'] * G.graph['cables']['cost'][edgeD['cable']]

        edge_crossing = check_crossings(G)
        if edge_crossing:
            print('WARNING: there is at least one cable crossing with the new coordinates')

        if G.graph['boundary'] is not None:
            inside_boundary = is_inside_boundary(boundary=G.graph['boundary'], coordinates=G.graph['VertexC'])
            if not inside_boundary:
                print('WARNING: at least one of the coordinates is outside the boundary')

        self.G_evaluate = G
        if update_selfG:
            self.G = G

        # Attach a plot method to the graph object
        def plot_graph(node_tag=None):
            ax = gplot(G, node_tag=node_tag)
            return ax
        G.plot = plot_graph

        if output_format == 'graph':
            output = G
        elif output_format == 'table':
            output = graph_to_table(G)
        elif output_format == 'yaml':
            output = graph_to_yaml(G)
        else:
            raise ValueError("Invalid output format. Supported formats: 'graph', 'table', 'yaml'")
        return output


class Constraints(dict):
    def __init__(self, **kwargs):
        dict.__init__(self, {'crossing': False,
                             'tree': False,
                             'thermal_capacity': False,
                             'number_of_main_feeders': False})
        self.update(kwargs)


def main():
    if __name__ == '__main__':
        # Turbine and substation positions from IEA37 Borssele Regular System
        substations_pos = np.asarray([[497620.7], [5730622.0]]).T
        turbines_pos = np.array([[500968.1461, 499748.6565, 501245.7744, 500026.2848,
                                  498527.8286, 497308.339, 501523.4027, 500303.9131,
                                  498805.4569, 497585.9673, 496087.5111, 494868.0215,
                                  501801.031, 500581.5414, 499083.0852, 497863.5956,
                                  496365.1394, 495145.6498, 493647.1936, 492427.704,
                                  502078.6593, 500859.1697, 499360.7135, 498141.2239,
                                  496642.7677, 495423.2781, 493924.8219, 492705.3323,
                                  491206.8762, 489987.3865, 502356.2876, 501136.798,
                                  499638.3418, 498418.8522, 496920.396, 495700.9064,
                                  494202.4502, 492982.9606, 491484.5045, 490265.0148,
                                  488766.5587, 487547.069, 502633.9159, 501414.4263,
                                  499915.9701, 498696.4805, 497198.0243, 495978.5347,
                                  494480.0786, 493260.5889, 491762.1328, 490542.6431,
                                  489044.187, 487824.6973, 486326.2412, 485106.7515,
                                  497475.6526, 496256.163, 494757.7069, 493538.2172,
                                  492039.7611, 490820.2714, 489321.8153, 488102.3256,
                                  486603.8695, 497753.2809, 496533.7913, 495035.3352,
                                  493815.8455, 492317.3894, 489599.4436, 498030.9093,
                                  496811.4196, 495312.9635],
                                 [5716452.784, 5717635.848, 5718427.22, 5719610.283,
                                  5718809.394, 5719992.458, 5720401.656, 5721584.719,
                                  5720783.83, 5721966.894, 5721166.004, 5722349.068,
                                  5722376.092, 5723559.155, 5722758.266, 5723941.33,
                                  5723140.44, 5724323.504, 5723522.615, 5724705.678,
                                  5724350.528, 5725533.591, 5724732.702, 5725915.765,
                                  5725114.876, 5726297.94, 5725497.051, 5726680.114,
                                  5725879.225, 5727062.288, 5726324.963, 5727508.027,
                                  5726707.138, 5727890.201, 5727089.312, 5728272.376,
                                  5727471.486, 5728654.55, 5727853.661, 5729036.724,
                                  5728235.835, 5729418.899, 5728299.399, 5729482.463,
                                  5728681.574, 5729864.637, 5729063.748, 5730246.812,
                                  5729445.922, 5730628.986, 5729828.097, 5731011.16,
                                  5730210.271, 5731393.335, 5730592.445, 5731775.509,
                                  5731038.184, 5732221.248, 5731420.358, 5732603.422,
                                  5731802.533, 5732985.596, 5732184.707, 5733367.77,
                                  5732566.881, 5733012.62, 5734195.683, 5733394.794,
                                  5734577.858, 5733776.968, 5734159.143, 5734987.056,
                                  5736170.119, 5735369.23]]).T
        BoundaryC = np.array(list(zip([484178.55, 500129.9, 497318.1,
                                       503163.37, 501266.5, 488951.0],
                                      [5732482.8, 5737534.4, 5731880.24,
                                       5729155.3, 5715990.05, 5727940.])))
        site_info = {'site_name': 'IEA-37 Regular',
                     'handle': 'iea37reg',
                     'boundary': BoundaryC
                     }
        cables = np.array([[500, 3, 206], [800, 5, 287], [1000, 7, 406]])

        wfn = WindFarmNetwork(turbines_pos=turbines_pos, substations_pos=substations_pos, cables=cables, site_info=site_info)

        # Optimize cable layout with the given data
        H1 = wfn.optimize(output_format='table')
        print(H1)
        H1.plot()

        # Evaluate the layout with new coordinates
        new_substations_pos = np.asarray([[499620.7], [5731622.0]]).T
        H2 = wfn.evaluate(substations_pos=new_substations_pos, update_selfG=True)
        H2.plot()
        plt.show()


main()
