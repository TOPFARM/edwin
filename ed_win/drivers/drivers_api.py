from pyomo import environ as pyo
import numpy as np
from .interarray.plotting import gplot
from .interarray.svg import svgplot
from .interarray.geometric import delaunay, make_graph_metrics
from .interarray.interarraylib import calcload, G_from_site
from .interarray.pathfinding import PathFinder
from .interarray.interface import assign_cables
from .interarray.heuristics import CPEW, OBEW


class InterArray:
    def __init__(self, G, solver_name='heuristic(cpew)', gap=0, timelimit=600, other_settings={}, cables=[]):
        make_graph_metrics(G)
        self.G = G
        self.solver_name = solver_name
        self.gap = gap
        self.timelimit = timelimit
        self.other_settings = other_settings
        cables_list = cables.tolist()
        self.cables = [(cx, capacity, cost) for cx, capacity, cost in cables_list]
        self.max_cap = max(cable[1] for cable in self.cables) if self.cables else 0

        # Run and get the result
        self.H = self.run(G)

    def run(self, G):
        solver = self._initialize_solver()
        if self.solver_name.startswith('heuristic'):
            return self._run_heuristic_solver()
        else:
            return self._run_optimization_solver(solver)

    def _initialize_solver(self):
        solver_name = self.solver_name
        other_settings = self.other_settings
        gap = self.gap
        timelimit = self.timelimit

        if solver_name == 'ortools(cp-sat)':
            from .interarray.MILP.ortools import cp_model
            solver = cp_model.CpSolver()
            solver.parameters.relative_gap_limit = gap
            solver.parameters.max_time_in_seconds = timelimit
            if other_settings.get('tee', False):
                solver.parameters.log_search_progress = True
                solver.log_callback = print
        elif solver_name == 'cplex':
            solver = pyo.SolverFactory(solver_name, solver_io='python')
            solver.options = {'mipgap': gap, 'timelimit': timelimit}
        elif solver_name == 'cbc':
            cbc_solver_path = other_settings.get('cbc_solver_path')
            if cbc_solver_path:
                solver = pyo.SolverFactory(solver_name, executable=cbc_solver_path)
            else:
                solver = pyo.SolverFactory(solver_name)
            solver.options = {'ratioGap': gap, 'seconds': timelimit, 'timeMode': 'elapsed',
                              'threads': other_settings.get('threads', 1)}
        elif solver_name == 'heuristic(cpew)' or 'heuristic(obew)':
            self.solver_name = solver_name
            solver = None
        else:
            print(f'Warning: The given solver name ({solver_name}) is not among the options. '
                  f'It will be solved by the default solver (heuristic(cpew))')
            self.solver_name = 'heuristic(cpew)'
            solver = None

        return solver

    def _run_heuristic_solver(self):
        solver_name = self.solver_name
        G = self.G
        max_cap = self.max_cap
        if solver_name == 'heuristic(cpew)':
            heuristic_solver = CPEW
        elif solver_name == 'heuristic(obew)':
            heuristic_solver = OBEW

        print(f'Solving with {solver_name}\n')
        H = heuristic_solver(G, max_cap)
        calcload(H)
        assign_cables(H, self.cables)
        self.H = H
        return H

    def _run_optimization_solver(self, solver):
        G = self.G
        max_cap = self.max_cap
        other_settings = self.other_settings
        if self.solver_name == 'ortools(cp-sat)':
            from .interarray.MILP.ortools import make_MILP_length, MILP_solution_to_G, MILP_warmstart_from_G, cp_model
        else:
            from .interarray.MILP.pyomo import make_MILP_length, MILP_solution_to_G, MILP_warmstart_from_G
        A = delaunay(G)
        m = make_MILP_length(A, max_cap,
                             gateXings_constraint=other_settings.get('gateXings_constraint', False),
                             branching=other_settings.get('branching', False),
                             gates_limit=other_settings.get('gates_limit', 0))

        if other_settings.get('warmstart', False):
            G_prime = CPEW(G, max_cap) if other_settings.get('gateXings_constraint', False) else OBEW(G, max_cap)
            calcload(G_prime)
            MILP_warmstart_from_G(m, G_prime)

        print(f'Solving with {self.solver_name}\n')
        if self.solver_name == 'ortools(cp-sat)':
            status = solver.Solve(m)
            print(solver.ResponseStats(),
                  f"\nbest solution's strategy: {solver.SolutionInfo()}",
                  f'\ngap: {100 * (solver.ObjectiveValue() - solver.BestObjectiveBound()) / solver.BestObjectiveBound():.1f}%')
        else:
            status = solver.solve(m, tee=other_settings.get('tee', False), warmstart=other_settings.get('warmstart', False))

        H = MILP_solution_to_G(m, solver=solver, A=A)

        if not other_settings.get('gateXings_constraint', False):
            H = PathFinder(H).create_detours(in_place=True)
            calcload(H)

        assign_cables(H, self.cables)
        return H
