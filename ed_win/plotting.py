# SPDX-License-Identifier: LGPL-2.1-or-later
# https://github.com/mdealencar/interarray

from collections import defaultdict
from collections.abc import Sequence
from typing import Optional

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from matplotlib import animation
from matplotlib.patches import Polygon
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar

from .plotting_scripts.geometric import make_graph_metrics, rotate
from .plotting_scripts.interarraylib import calcload


FONTSIZE_LABEL = 6
FONTSIZE_LOAD = 8
FONTSIZE_ROOT_LABEL = 6
FONTSIZE_LEGEND_BOX = 7
FONTSIZE_LEGEND_STRIP = 6
NODESIZE = 18
NODESIZE_LABELED = 70
NODESIZE_LABELED_ROOT = 28
NODESIZE_DETOUR = 80
NODESIZE_LABELED_DETOUR = 150


# Define the configurations
rcParams = {
    'figure.constrained_layout.use': True,
    'figure.facecolor': 'white',
    'savefig.facecolor': 'white',
    'savefig.transparent': True,
    'figure.dpi': 192,
    'figure.frameon': False,
    'figure.edgecolor': 'none',  # Corrected value
    'savefig.bbox': 'tight'
}

# Update Matplotlib rcParams with defined configurations
plt.rcParams.update(rcParams)


def gplot(G, ax=None, node_tag=None, edge_exemption=False, figlims=(5, 6),
          landscape=True, infobox=True):
    '''NetworkX graph plotting function.
    `node_tag` in [None, 'load', 'label']
    (or other key in nodes's dict)'''
    figsize = plt.rcParams['figure.figsize']
    dark = plt.rcParams['figure.facecolor'] != 'white'

    root_size = NODESIZE_LABELED_ROOT if node_tag is not None else NODESIZE
    detour_size = NODESIZE_LABELED_DETOUR if node_tag is not None else NODESIZE_DETOUR
    node_size = NODESIZE_LABELED if node_tag is not None else NODESIZE

    type2color = {}
    type2style = dict(
        detour='dashed',
        scaffold='dotted',
        extended='dashed',
        delaunay='solid',
        unspecified='solid',
    )
    if dark:
        scalebar = False
        type2color.update(
            detour='darkorange',
            scaffold='gray',
            delaunay='darkcyan',
            extended='darkcyan',
            unspecified='crimson',
        )
        root_color = 'lawngreen'
        node_edge = 'none'
        detour_ring = 'orange'
        polygon_edge = 'none'
        polygon_face = '#111111'
    else:
        scalebar = True
        type2color.update(
            detour='royalblue',
            scaffold='gray',
            delaunay='black',
            extended='black',
            unspecified='firebrick',
        )
        root_color = 'black' if node_tag is None else 'yellow'
        node_edge = 'black'
        detour_ring = 'deepskyblue'
        polygon_edge = '#444444'
        polygon_face = 'whitesmoke'

    landscape_angle = G.graph.get('landscape_angle')
    if landscape and landscape_angle:
        # landscape_angle is not None and not 0
        VertexC = rotate(G.graph['VertexC'], landscape_angle)
    else:
        VertexC = G.graph['VertexC']
    M = G.graph['M']
    N = G.number_of_nodes() - M
    D = G.graph.get('D')

    # draw farm boundary
    if 'boundary' in G.graph and G.graph['boundary'] is not None:
        BoundaryC = (rotate(G.graph['boundary'], landscape_angle)
                     if landscape and landscape_angle else
                     G.graph['boundary'])
        if ax is None and not dark:
            limX, limY = figlims
            r = limX / limY
            XYrange = np.abs(np.amax(BoundaryC, axis=0) -
                             np.amin(BoundaryC, axis=0))
            d = XYrange[0] / XYrange[1]
            if d < r:
                figsize = (limY * d, limY)
            else:
                figsize = (limX, limX / d)

        area_polygon = Polygon(BoundaryC, zorder=0, linestyle='--',
                               facecolor=polygon_face, edgecolor=polygon_edge,
                               linewidth=0.3)
        if ax is None:
            fig, ax = plt.subplots(figsize=figsize)
        ax.add_patch(area_polygon)
        ax.update_datalim(area_polygon.get_xy())
        ax.autoscale()
    elif ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    ax.axis('off')

    ax.set_aspect('equal')
    # setup
    roots = range(-M, 0)
    pos = dict(zip(range(N), VertexC[:N])) | dict(zip(roots, VertexC[-M:]))
    if D is not None:
        N -= D
        fnT = G.graph.get('fnT')
        detour = range(N, N + D)
        pos |= dict(zip(detour, VertexC[fnT[detour]]))
    RootL = {r: G.nodes[r]['label'] for r in roots[::-1]}

    colors = plt.cm.get_cmap('tab20', 20).colors
    # default value for subtree (i.e. color for unconnected nodes)
    # is the last color of the tab20 colormap (i.e. 19)
    subtrees = G.nodes(data='subtree', default=19)
    node_colors = [colors[subtrees[n] % len(colors)] for n in range(N)]

    cables_capacity = G.graph['cables']['capacity']

    # draw edges
    edges = G.edges(data='cable')
    cable_types = [edge[-1] for edge in edges]
    cable_types_unique = list(set(cable_types))

    # draw edges without type
    for cable_type in cable_types_unique:
        nx.draw_networkx_edges(G, pos, ax=ax, edge_color=type2color['unspecified'],
                               style=type2style['unspecified'], label=f"{int(G.graph['cables']['area'][cable_type])} mm²",
                               edgelist=[(u, v) for u, v, data in G.edges(data=True)
                                         if data['cable'] == cable_type and data.get('type', None) is None], width=(cable_type + 0.5) / 2)

    # draw edges with type
    for edge_type in type2style:
        edge_data = [(u, v, data['cable']) for u, v, data in G.edges(data=True) if data.get('type', None) == edge_type]
        nx.draw_networkx_edges(G, pos, ax=ax, edge_color=type2color[edge_type],
                               style=type2style[edge_type], label=edge_type,
                               edgelist=[(u, v) for u, v, data in edge_data],
                               width=[(cable_type + 0.5) / 2 for _, _, cable_type in edge_data])

    # draw nodes
    if D is not None:
        # draw circunferences around nodes that have Detour clones
        nx.draw_networkx_nodes(G, pos, ax=ax, nodelist=detour, alpha=0.4,
                               edgecolors=detour_ring, node_color='none',
                               node_size=detour_size,
                               label='corner')
    nx.draw_networkx_nodes(G, pos, ax=ax, nodelist=roots, linewidths=0.2,
                           node_color=root_color, edgecolors=node_edge,
                           node_size=root_size, node_shape='s',
                           label='OSS')
    nx.draw_networkx_nodes(G, pos, nodelist=range(N), edgecolors=node_edge,
                           ax=ax, node_color=node_colors, node_size=node_size,
                           linewidths=0.2, label='WTG')

    # draw labels
    font_size = dict(load=FONTSIZE_LOAD,
                     label=FONTSIZE_LABEL,
                     tag=FONTSIZE_ROOT_LABEL)
    if node_tag is not None:
        if node_tag == 'load' and 'has_loads' not in G.graph:
            node_tag = 'label'
        labels = nx.get_node_attributes(G, node_tag)
        for root in roots:
            if root in labels:
                labels.pop(root)
        if D is not None:
            for det in detour:
                if det in labels:
                    labels.pop(det)
        nx.draw_networkx_labels(G, pos, ax=ax, font_size=font_size[node_tag],
                                labels=labels)
    # root nodes' labels
    if node_tag is not None:
        nx.draw_networkx_labels(G, pos, ax=ax, font_size=FONTSIZE_ROOT_LABEL,
                                labels=RootL)

    if scalebar:
        bar = AnchoredSizeBar(ax.transData, 1000, '1 km', 'lower right',
                              frameon=False)
        ax.add_artist(bar)

    if infobox:
        info = []
        if 'capacity' in G.graph:
            info = [f'$n_{{\\mathrm{{max}}}}$ = {G.graph["capacity"]}, '
                    f'$N$ = {N}']
            feeder_info = [f'$\\phi_{{{rootL}}}$ = {len(G[r])}'
                           for r, rootL in RootL.items()]
            if 'overfed' in G.graph:
                feeder_info = [fi + f' ({100*(overfed - 1):+.0f}%)'
                               for fi, overfed in
                               zip(feeder_info, G.graph['overfed'][::-1])]
            info.extend(feeder_info)
            # legend.append(', '.join(feeder_info))
            Gʹ = nx.subgraph_view(G,
                                  filter_edge=lambda u, v: 'length' in G[u][v])
            length = Gʹ.size(weight="length")
            intdigits = int(np.floor(np.log10(length))) + 1
            info.append(f'Σl = {round(length, max(0, 5 - intdigits))} m')
            #  assert Gʹ.number_of_edges() == G.number_of_nodes() - 1, \
            #          f'{Gʹ.number_of_edges()} != {G.number_of_nodes()}'
            # for field, sym in (('weight', 'w'), ('length', 'l')):
            #  for field, sym in (('length', ''),):
            #      weight = field if all([(field in data)
            #                             for _, _, data in G.edges.data()]) else None
            #      legend.append('Σ{} = {:.0f}'.format(sym, G.size(weight=weight)) +
            #                    ' m' if field == 'length' else '')
        if ('has_costs' in G.graph):
            info.append('{:.0f} €'.format(G.size(weight='cost')))
        if 'capacity' in G.graph:
            infobox = ax.legend([], fontsize=FONTSIZE_LEGEND_BOX,
                                title='\n'.join(info), labelspacing=0)
            # loc='upper right'
            # bbox_to_anchor=(-0.04, 0.80, 1.08, 0)
            # bbox_to_anchor=(-0.04, 1.03, 1.08, 0)
            plt.setp(infobox.get_title(), multialignment='center')
            # ax.legend(title='\n'.join(legend))
            # legend1 = pyplot.legend(plot_lines[0], ["algo1", "algo2", "algo3"], loc=1)
            # pyplot.legend([l[0] for l in plot_lines], parameters, loc=4)
            # ax.add_artist(legstrip)
    if not dark:
        ax.legend(ncol=8, fontsize=FONTSIZE_LEGEND_STRIP, loc='lower center',
                  frameon=False, bbox_to_anchor=(0.5, -0.07),
                  columnspacing=1, handletextpad=0.3)
        if 'capacity' in G.graph and infobox:
            ax.add_artist(infobox)

    return ax
