# SPDX-License-Identifier: LGPL-2.1-or-later
# https://github.com/mdealencar/interarray

# author, version, license, and long description
__version__ = '0.0.1'
__author__ = 'Mauricio Souza de Alencar'

# global module constants
MAX_TRIANGLE_ASPECT_RATIO = 15.0
