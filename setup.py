# -*- coding: utf-8 -*-
"""
Setup file for EDWIN
"""
import os
from setuptools import setup, find_packages
import pkg_resources

repo = os.path.dirname(__file__)
try:
    from git_utils import write_vers
    version = '1.1.0'  # write_vers(vers_file='ed_win/__init__.py', repo=repo, skip_chars=1)
except Exception:
    version = '999.0.0'


try:
    from pypandoc import convert_file

    def read_md(f): return convert_file(f, 'rst', format='md')
except ImportError:
    print("warning: pypandoc module not found, could not convert Markdown to RST")

    def read_md(f): return open(f, 'r').read()


setup(name='ed_win',
      version=version,
      description='EDWIN an optimization and design package for electrical networks in windfarms',
      long_description=read_md('README.md'),
      url='https://gitlab.windenergy.dtu.dk/TOPFARM/EDWIN',
      author='DTU Wind Energy',
      author_email='amia@dtu.dk',
      license='MIT',
      packages=find_packages() + ['ed_win.drivers'],
      install_requires=[
          'networkx',  # plotting
          'matplotlib',  # for plotting
          'shapely',  # for geometric calculus
          'numpy',  # for numerical calculations
          'xarray',  # for WaspGridSite data storage
          'scipy',  # constraints
          'windIO',
      ],
      extras_require={
          'test': [
              'pytest',  # for testing
              'pytest-cov',  # for calculating coverage
              'sphinx',  # generating documentation
              'sphinx_rtd_theme',  # docs theme
          ],
          'interarray': [
              'openpyxl',
              'pony',
              'pyyaml',
              'utm',
              'pyomo',
              'loguru',
              'numba',
              'ortools',
              'svg.py',
          ]},
      zip_safe=True)
